const timer = document.querySelector('.time'),
	squares = document.querySelector('.container').querySelectorAll('.square'),
	button = document.querySelector('#start'),
	result = document.querySelector('#result'),
	radioBlue = document.querySelector('#blue'),
	radioRed = document.querySelector('#red'),
	textRadioBlue = document.querySelector('#textBlue'),
	textRadioRed = document.querySelector('#textRed'),
	lastWinnerText = document.querySelector('#lastWinner'),
	playedCount = document.querySelector('#playedCount'),
	blueCount = document.querySelector('#blueCount'),
	redCount = document.querySelector('#redCount'),
	oldCount = document.querySelector('#oldCount'),
	loadEffect = setInterval(() => {
		if (result.textContent.substring(0, 10) == 'Aguardando') {
			if (result.textContent == 'Aguardando...') {
				result.textContent = 'Aguardando'
			}
			else {
				result.textContent += '.'
			}
		}
	}, 220)

let interval,
	seconds = 0,
	player = 0,
	totalPlayed = 0,
	bluePlayed = 0,
	redPlayed = 0,
	oldPlayed = 0,
	status = 0,
	camp = [
		[0, 0, 0],
		[0, 0, 0],
		[0, 0, 0]
	],
	identifier = [
		[0, 1, 2],
		[3, 4, 5],
		[6, 7, 8]
	],
	lastWinner = '',
	eneagon = [
		[0, 0],
		[0, 1],
		[0, 2],
		[1, 0],
		[1, 1],
		[1, 2],
		[2, 0],
		[2, 1],
		[2, 2]
	]

function getTime(seconds) {
	const hours = parseInt(seconds / 3600, 10)
	seconds -= hours * 3600
	const minutes = parseInt(seconds / 60)
	seconds -= minutes * 60

	return `${hours < 10 ? `0${hours}` : hours}:${minutes < 10 ? `0${minutes}` : minutes}:${seconds < 10 ? `0${seconds}` : seconds}`
}

function startGame() {
	if (interval == null) {
		console.log('Jogo iniciado')
		timer.textContent = 'Tempo: 00:00:00'
		player = radioBlue.checked ? 1 : 2
		status = 1
		button.firstElementChild.textContent = 'Terminar'
		result.innerHTML = player == 1 ? 'Jogador: <em style="color: blue">Azul</em>' :
			'Jogador: <em style="color: red">Vermelho</em>'
		camp = [
			[0, 0, 0],
			[0, 0, 0],
			[0, 0, 0]
		]
		radioBlue.disabled = true
		radioRed.disabled = true
		radioBlue.style.cursor = 'default'
		radioRed.style.cursor = 'default'
		textRadioBlue.style.cursor = 'default'
		textRadioRed.style.cursor = 'default'

		for (let item of squares) {
			if (item.style.backgroundColor != 'rgb(225, 251, 255)') {
				item.style.transition = 'background-color .1s, transform .25s'
				for (let i = 180; i > 0; i--) {
					item.style.transform = `rotateY(${i}deg)`
				}
				item.style.backgroundColor = 'rgb(225, 251, 255)'
			}
			item.style.cursor = 'pointer'
			item.style.animationName = 'none'
		}

		interval = setInterval(() => {
			timer.textContent = 'Tempo: ' + getTime(++seconds)
		}, 1000)

	} else console.log('Jogo já iniciado')
}

function endGame() {
	clearInterval(interval)
	console.log('Jogo parado.')
	console.log(`Tempo decorrido: ${getTime(seconds)}`)
	seconds = 0
	player = 0
	status = 0
	interval = null
	radioBlue.disabled = false
	radioRed.disabled = false
	radioBlue.style.cursor = 'pointer'
	radioRed.style.cursor = 'pointer'
	textRadioBlue.style.cursor = 'pointer'
	textRadioRed.style.cursor = 'pointer'
	for (let item of squares) {
		if (item.style.cursor == 'pointer') {
			item.style.cursor = 'default'
		}
	}
}

document.addEventListener('contextmenu', e => {
	e.preventDefault()
})

function findWinner(x, winner, color, id) {
	if (camp[x][0] == id && camp[x][1] == id && camp[x][2] == id) {
		for (let i = 0; i < 3; i++) {
			squares[identifier[x][i]].style.animationName = `${color}Win`
		}
		endGame()
	} else if (camp[0][x] == id && camp[1][x] == id && camp[2][x] == id) {
		for (let i = 0; i < 3; i++) {
			squares[identifier[i][x]].style.animationName = `${color}Win`
		}
		endGame()
	} else if (camp[0][0] == id && camp[1][1] == id && camp[2][2] == id) {
		squares[identifier[0][0]].style.animationName = `${color}Win`
		squares[identifier[1][1]].style.animationName = `${color}Win`
		squares[identifier[2][2]].style.animationName = `${color}Win`
		endGame()
	} else if (camp[0][2] == id && camp[1][1] == id && camp[2][0] == id) {
		squares[identifier[0][2]].style.animationName = `${color}Win`
		squares[identifier[1][1]].style.animationName = `${color}Win`
		squares[identifier[2][0]].style.animationName = `${color}Win`
		endGame()
	}

	if (status == 0) {
		lastWinner = winner
		if (winner == 'Azul') {
			blueCount.innerHTML = 'Placar <em style="color: blue">Azul</em>: ' + ++bluePlayed
		}
		else {
			redCount.innerHTML = 'Placar <em style="color: red">Vermelho</em>: ' + ++redPlayed
		}

		result.innerHTML = `Vencedor: <em style="color: ${color}; cursor: pointer" onclick="alert('Parabéns, time ${lastWinner} :)')">` + winner + '</em>!'
		lastWinnerText.innerHTML = `Último vencedor: <em style="color: ${color}; cursor: pointer" onclick="alert('Time ${lastWinner} foi o último vencedor!')">${winner}</em>`
	}
}

function verifyWinner() {
	if (status) { findWinner(0, 'Azul', 'blue', 1) }
	if (status) { findWinner(1, 'Azul', 'blue', 1) }
	if (status) { findWinner(2, 'Azul', 'blue', 1) }

	if (status) { findWinner(0, 'Vermelho', 'red', 2) }
	if (status) { findWinner(1, 'Vermelho', 'red', 2) }
	if (status) { findWinner(2, 'Vermelho', 'red', 2) }

	if (status == 1 && camp[0][0] != 0 && camp[0][1] != 0 && camp[0][2] != 0 && camp[1][0] != 0 && camp[1][1] != 0 && camp[1][2] != 0 && camp[2][0] != 0 && camp[2][1] != 0 && camp[2][2] != 0) {
		result.innerHTML = 'Deu <em style="color: yellow; cursor: pointer" onclick="alert(\'Deu velha! Haha\')">Velha</em>, haha!'
		endGame()
		oldCount.innerHTML = 'Placar <em style="color: yellow">Velha</em>: ' + ++oldPlayed
	}
	if (status == 0) {
		playedCount.textContent = 'Total de jogos: ' + ++totalPlayed
		button.setAttribute('title', 'Reiniciar')
		button.firstElementChild.textContent = 'Reiniciar'
	}
}

for (let i = 0; i < squares.length; i++) {
	squares[i].addEventListener('click', () => {
		if (status == 1) {
			squares[i].style.cursor = 'default'
			squares[i].style.transition = 'background-color .8s, transform .25s'
			for (let j = 0; j < 180; j++)
				squares[i].style.transform = `rotateY(${j}deg)`

			if (player == 1 && camp[eneagon[i][0]][eneagon[i][1]] == 0) {
				camp[eneagon[i][0]][eneagon[i][1]] = 1
				squares[i].style.backgroundColor = 'blue'
				player = 2
				result.innerHTML = 'Jogador: <em style="color: red">Vermelho</em>'
			} else if (player == 2 && camp[eneagon[i][0]][eneagon[i][1]] == 0) {
				camp[eneagon[i][0]][eneagon[i][1]] = 2
				squares[i].style.backgroundColor = 'red'
				player = 1
				result.innerHTML = 'Jogador: <em style="color: blue">Azul</em>'
			}

			verifyWinner()
		}
	})
}

button.addEventListener('click', () => {
	if (status == 0) {
		startGame()
		button.setAttribute('title', 'Terminar')
	}
	else {
		endGame()
		button.setAttribute('title', 'Começar')
		button.firstElementChild.textContent = 'Começar'
		result.textContent = 'Aguardando'
	}
})
